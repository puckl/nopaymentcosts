<?php

/*
 *   *********************************************************************************************
 *      Please retain this copyright header in all versions of the software.
 *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
 *
 *      Copyright (C) Josef A. Puckl | eComStyle.de
 *      All rights reserved - Alle Rechte vorbehalten
 *
 *      This commercial product must be properly licensed before being used!
 *      Please contact info@ecomstyle.de for more information.
 *
 *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
 *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
 *   *********************************************************************************************
 */

namespace Ecs\NoPaymentCosts\Model;

class Payment extends Payment_parent
{

    public function getPrice()
    {
        $myConfig = $this->getConfig();
        $oActView = $myConfig->getActiveView();
        if (isAdmin() or $this->oxpayments__oxaddsum->value or !$oActView->getClassName() == "payment") {
            return parent::getPrice();
        } else {
            return null;
        }
    }
}
